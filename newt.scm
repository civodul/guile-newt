(define-module (newt))

(eval-when (eval load compile)
  (begin
    (define %public-modules
      '((newt bindings)
        (newt checkboxtree)
        (newt components)
        (newt entry)
        (newt form)
        (newt grid)
        (newt listbox)
        (newt scale)
        (newt textbox)
        (newt window)))

    (for-each (let ((i (module-public-interface (current-module))))
                (lambda (m)
                  (module-use! i (resolve-interface m))))
              %public-modules)))

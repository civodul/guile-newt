(use-modules (newt bindings)
             (newt components)
             (newt form)
             (newt listbox)
             (newt window))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")
(make-window 10 5 40 25 "Listbox Sample")

(define lb1 (make-listbox 1 1 10 (logior FLAG-SCROLL FLAG-BORDER)))
(define f1 (make-form))

(add-component-to-form f1 lb1)
(define key1 (append-entry-to-listbox lb1 "ENTRY1"))
(define key2 (append-entry-to-listbox lb1 "ENTRY2"))
(define key3 (append-entry-to-listbox lb1 "ENTRY3"))
(define key4 (append-entry-to-listbox lb1 "ENTRY4"))

(define b1 (make-button 1 20 "Ok"))

(add-component-to-form f1 b1)
(set-current-listbox-entry-by-key lb1 key1)
(set-listbox-entry-text lb1 1 "ENTRY1 EDITED")
(set-listbox-width lb1 30)
(insert-entry-to-listbox lb1 "ENTRY2 INSERTED" key2)
(delete-listbox-entry lb1 key2)

(run-form f1)

(newt-finish)

(use-modules (newt bindings)
             (newt components)
             (newt form)
             (newt window))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")
(make-window 10 5 40 25 "Button Sample")

(define r1 (make-radio-button 1 3 "Choice 1" #t))
(define r2 (make-radio-button 1 4 "Choice 2" #f r1))
(define r3 (make-radio-button 1 5 "Choice 3" #f r2))
(define cb1 (make-checkbox 1 7 "Choice" #\a "abcdef"))
(define b1 (make-button 1 10 "Ok"))
(define l1 (make-label 1 18 "LABEL"))

(define f1 (make-form))

(add-components-to-form f1 r1 r2 r3 cb1 b1 l1)

(set-current-radio-button r3)
(set-checkbox-flags cb1 FLAG-DISABLED FLAG-ROLE-SET)
(set-label-text l1 "LABEL CHANGED")

(run-form f1)

(newt-finish)

(use-modules (newt bindings)
             (newt components)
             (newt checkboxtree)
             (newt entry)
             (newt form)
             (newt grid)
             (newt listbox)
             (newt scale)
             (newt textbox)
             (newt window)
             (srfi srfi-1))

(newt-init)
(clear-screen)

(push-help-line "This is the help line")

(message-window "TITLE" "Ok" "Sample message window")
(choice-window  "TITLE" "Ok" "Cancel" "Sample choice window")
(ternary-window "TITLE" "Ok" "Cancel" "Quit" "Sample ternary window")

(newt-finish)

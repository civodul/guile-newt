(use-modules (newt bindings)
             (newt components)
             (newt checkboxtree)
             (newt entry)
             (newt form)
             (newt grid)
             (newt listbox)
             (newt scale)
             (newt textbox)
             (newt window)
             (srfi srfi-1))

(newt-init)
(clear-screen)

(define b1 (make-button -1 -1 "Ok"))
(define b2 (make-button -1 -1 "Cancel"))
(define b3 (make-button -1 -1 "Test"))

(define l1 (make-label -1 -1 "Basic window grid"))

(define g1 (horizontal-stacked-grid GRID-ELEMENT-COMPONENT b1))

(define g2 (horizontal-stacked-grid GRID-ELEMENT-COMPONENT b2
                                    GRID-ELEMENT-COMPONENT b3))
(define f1 (make-form))

(define g3 (basic-window-grid l1 g1 g2))

(add-components-to-form f1 l1 b1 b2 b3)
(make-wrapped-grid-window g3 "GRID")

(run-form f1)

(newt-finish)

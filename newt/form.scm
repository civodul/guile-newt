;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;; Copyright © 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt form)
  #:use-module (system foreign)
  #:use-module (ice-9 match)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-form
            set-form-timer
            set-current-component
            add-component-to-form
            add-components-to-form

            run-form
            draw-form
            form-add-hotkey
            form-watch-fd
            destroy-form

            FD-READ
            FD-WRITE
            FD-EXCEPT

            KEY-EXTRA-BASE
            KEY-UP
            KEY-DOWN
            KEY-LEFT
            KEY-RIGHT
            KEY-BKSPC
            KEY-DELETE
            KEY-HOME
            KEY-END
            KEY-UNTAB
            KEY-PGUP
            KEY-PGDN
            KEY-INSERT))

(define KEY-EXTRA-BASE #x8000)
(define KEY-UP         #x8001)
(define KEY-DOWN       #x8002)
(define KEY-LEFT       #x8004)
(define KEY-RIGHT      #x8005)
(define KEY-BKSPC      #x8006)
(define KEY-DELETE		 #x8007)
(define KEY-HOME       #x8008)
(define KEY-END        #x8009)
(define KEY-UNTAB      #x800A)
(define KEY-PGUP       #x800B)
(define KEY-PGDN       #x800C)
(define KEY-INSERT		 #x800D)

(define* (make-form #:key (flags 0))
  "Create a FORM with the given FLAGS. Forms are collections of
components. Only one form can be active at a time and every component
which the user should be able to access must be on the running form."
  (let ((proc (libnewt->procedure '*
                                  "newtForm"
                                  `(* * ,int))))
    (pointer->form (proc %null-pointer %null-pointer flags))))

(define (set-form-timer form delay-ms)
  "Set a timer to expire in DELAY-MS milliseconds for given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtFormSetTimer"
                                  `(* ,int))))
    (proc (form->pointer form) delay-ms)))

(define (set-current-component form component)
  "Set COMPONENT as the current component in the given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtFormSetCurrent"
                                  '(* *))))
    (proc (form->pointer form) (generic-component->pointer component))))

(define (add-component-to-form form component)
  "Add COMPONENT to the given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtFormAddComponent"
                                  '(* *))))
    (proc (form->pointer form) (generic-component->pointer component))))

(define (add-components-to-form form . components)
  "Add COMPONENTS to the given FORM."
  (let* ((count (length components))
         (prototype-components (map (lambda _ '*) (iota count)))
         (prototype (append prototype-components '(* *)))
         (proc (libnewt->procedure void
                                   "newtFormAddComponents"
                                   prototype))
         (c-components (map (lambda (component)
                              (generic-component->pointer component))
                            components)))
    (apply proc `(,(form->pointer form) ,@c-components ,%null-pointer))))

(define EXIT-HOTKEY    0)
(define EXIT-COMPONENT 1)
(define EXIT-FDREADY   2)
(define EXIT-TIMER     3)
(define EXIT-ERROR     4)

(define (key->symbol key)
  (case key
    ((#x8000) 'key-extra-base)
    ((#x8001) 'key-up)
    ((#x8002) 'key-down)
    ((#x8004) 'key-left)
    ((#x8005) 'key-right)
    ((#x8006) 'key-bkspc)
    ((#x8007) 'key-delete)
    ((#x8008) 'key-home)
    ((#x8009) 'key-end)
    ((#x800A) 'key-untab)
    ((#x800B) 'key-pgup)
    ((#x800C) 'key-pgdn)
    ((#x800D) 'key-insert)))

(define (run-form form)
  "Run the given FORM. Return the exit code of the form as two
values. The first is the exit reason and the second an argument
depending of the exit reason."
  (let* ((proc (libnewt->procedure void
                                   "newtFormRun"
                                   '(* *)))
         (exit-code-struct (list int '*))
         (exit-code (make-c-struct exit-code-struct
                                   (list 0 %null-pointer))))
    (proc (form->pointer form) exit-code)
    (match (parse-c-struct exit-code exit-code-struct)
      ((exit-reason argument)
       (cond
        ((= exit-reason EXIT-COMPONENT)
         (values 'exit-component (pointer->component argument)))
        ((= exit-reason EXIT-TIMER)
         (values 'exit-timer 0))
        ((= exit-reason EXIT-HOTKEY)
         (values 'exit-hotkey (key->symbol
                               (pointer-address argument))))
        ((= exit-reason EXIT-FDREADY)
         (values 'exit-fd-ready (pointer-address argument)))
        (else (values 'exit-unknown argument)))))))

(define (draw-form form)
  "Force the drawing of the given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtDrawForm"
                                  '(*))))
    (proc (form->pointer form))))

(define (form-add-hotkey form key)
  "Add KEY to FORM supported hotkeys."
  (let ((proc (libnewt->procedure void
                                  "newtFormAddHotKey"
                                  `(* ,int))))
    (proc (form->pointer form) key)))

;; Flags for 'newtFormWatchFd'.
(define FD-READ 1)
(define FD-WRITE 2)
(define FD-EXCEPT 4)

(define (form-watch-fd form fd flags)
  "Have FORM watch for events of type FLAGS on FD, a file descriptor.  FORM
exits as soon as an event occurs.  FLAGS must be a bitwise or of FD-READ,
FD-WRITE, and FD-EXCEPT."
  (let ((proc (libnewt->procedure void
                                  "newtFormWatchFd"
                                  `(* ,int ,int))))
    (proc (form->pointer form) fd flags)))

(define (destroy-form form)
  "Destroy the given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtFormDestroy"
                                  '(*))))
    (proc (form->pointer form))))

;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt components)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-compact-button
            make-button

            make-radio-button
            current-radio-button
            set-current-radio-button

            make-checkbox
            checkbox-value
            set-checkbox-value
            set-checkbox-flags

            FLAG-RETURNEXIT
            FLAG-HIDDEN
            FLAG-SCROLL
            FLAG-DISABLED
            FLAG-BORDER
            FLAG-WRAP
            FLAG-NOF12
            FLAG-MULTIPLE
            FLAG-SELECTED
            FLAG-CHECKBOX
            FLAG-PASSWORD
            FLAG-SHOWCURSOR

            FLAG-ROLE-SET
            FLAG-ROLE-RESET
            FLAG-ROLE-TOGGLE

            make-label
            set-label-text
            set-label-colors
            components=?
            add-component-callback))

;; Newt C library considers every almost every object as a
;; "Component". Because some functions are dedicated to a certain type of
;; objects (grid functions for instance), Guile-Newt introduces higher
;; level types for grids, forms, listboxes to avoid passing a grid to a
;; function dealing with forms. However, some objects without dedicated
;; functions are kept as "Component".

(define (make-compact-button left top text)
  "Create a compact button at (LEFT, TOP) coordinates, with given TEXT."
  (let ((proc (libnewt->procedure '*
                                  "newtCompactButton"
                                  `(,int ,int *))))
    (pointer->component (proc left top (string->pointer text)))))

(define (make-button left top text)
  "Create a button at (LEFT, TOP) coordinates, with given TEXT."
  (let ((proc (libnewt->procedure '*
                                  "newtButton"
                                  `(,int ,int *))))
    (pointer->component (proc left top (string->pointer text)))))

(define* (make-radio-button left top text default? #:optional previous-button)
  "Create a radio button at (LEFT, TOP) coordinates, with given
TEXT. Radio buttons can be linked together using PREVIOUS-BUTTON to
specify the previous radio button. PREVIOUS-BUTTON has to be omitted
for the first button of the serie. DEFAULT? indicates the initially
selected radio button."
  (let ((proc (libnewt->procedure '*
                                  "newtRadiobutton"
                                  `(,int ,int * ,int *))))
    (pointer->component (proc left top (string->pointer text)
                              (if default? 1 0)
                              (if previous-button
                                  (component->pointer previous-button)
                                  %null-pointer)))))
(define (current-radio-button radio-button)
  "Return the selected radio button in the given set of radio
buttons. RADIO-BUTTON can be any button of the set."
  (let ((proc (libnewt->procedure '*
                                  "newtRadioGetCurrent"
                                  '(*))))
    (pointer->component (proc (component->pointer radio-button)))))

(define (set-current-radio-button radio-button)
  "Set the given RADIO-BUTTON as selected."
  (let ((proc (libnewt->procedure void
                                  "newtRadioSetCurrent"
                                  '(*))))
    (proc (component->pointer radio-button))))

(define (make-checkbox left top text default-value sequence)
  "Create a checkbox at (LEFT, TOP) coordinates, with given TEXT as
label. DEFAULT-VALUE is the initial value of the checkbox and SEQUENCE
is a string which every character is proposed as a checkbox value when
toggling with <SPACE> key."
  (let ((proc (libnewt->procedure '*
                                  "newtCheckbox"
                                  `(,int ,int * ,int8 * *))))
    (pointer->checkbox (proc left top (string->pointer text)
                             (char->integer default-value)
                             (string->pointer sequence)
                             %null-pointer))))
(define (checkbox-value checkbox)
  "Return the current CHECKBOX value as a character."
  (let ((proc (libnewt->procedure int8
                                  "newtCheckboxGetValue"
                                  '(*))))
    (integer->char (proc (checkbox->pointer checkbox)))))

(define (set-checkbox-value checkbox value)
  "Set the CHECKBOX value to VALUE character."
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxSetValue"
                                  `(* ,int8))))
    (proc (checkbox->pointer checkbox) (char->integer value))))

(define FLAG-ROLE-SET    0)
(define FLAG-ROLE-RESET  1)
(define FLAG-ROLE-TOGGLE 2)

(define FLAG-RETURNEXIT 1)
(define FLAG-HIDDEN     2)
(define FLAG-SCROLL     4)
(define FLAG-DISABLED   8)
(define FLAG-BORDER     32)
(define FLAG-WRAP       64)
(define FLAG-NOF12      128)
(define FLAG-MULTIPLE   256)
(define FLAG-SELECTED   512)
(define FLAG-CHECKBOX   1024)
(define FLAG-PASSWORD   2048)
(define FLAG-SHOWCURSOR 4096)

(define (set-checkbox-flags checkbox flags flag-role)
  "Apply the given FLAGS to CHECKBOX. The flag role (set, reset or
toggle) is specified bu FLAG-ROLE."
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxSetFlags"
                                  `(* ,int ,int))))
    (proc (checkbox->pointer checkbox) flags flag-role)))

(define (make-label left top text)
  "Create a label at (LEFT, TOP) coordinates, with given TEXT."
  (let ((proc (libnewt->procedure '*
                                  "newtLabel"
                                  `(,int ,int *))))
    (pointer->label (proc left top (string->pointer text)))))

(define (set-label-text label text)
  "Set the TEXT of given LABEL."
  (let ((proc (libnewt->procedure void
                                  "newtLabelSetText"
                                  '(* *))))
    (proc (label->pointer label) (string->pointer text))))

(define (set-label-colors label colorset)
  "Set the COLORSET of given LABEL."
  (let ((proc (libnewt->procedure void
                                  "newtLabelSetColors"
                                  `(* ,int))))
    (proc (label->pointer label) colorset)))

(define (components=? component1 component2)
  "Compare equality of given COMPONENT1 and COMPONENT2. Note that
higher level types like grids are also accepted. It is useful when a
Newt function is returning a component we ignore the type but we still
want to compare it to higher level components created by Guile-newt."
  (= (pointer-address (generic-component->pointer component1))
     (pointer-address (generic-component->pointer component2))))

(define component-callback
  (lambda (callback)
    (procedure->pointer
     void
     (lambda (component data)  ; DATA is %null-pointer for now.
       (callback component))
     '(* *))))

(define (add-component-callback component callback)
  "Set CALLBACK as the callback procedure for COMPONENT."
  (let ((proc (libnewt->procedure void
                                  "newtComponentAddCallback"
                                  `(* * *))))
    (proc (generic-component->pointer component)
          (component-callback callback)
          %null-pointer)))

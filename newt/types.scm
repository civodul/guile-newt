;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt types)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:export (component? pointer->component component->pointer
            checkbox? pointer->checkbox checkbox->pointer
            checkboxtree? pointer->checkboxtree checkboxtree->pointer
            checkboxtree-key? pointer->checkboxtree-key checkboxtree-key->pointer
            entry? pointer->entry entry->pointer
            form? pointer->form form->pointer
            grid? pointer->grid grid->pointer
            label? pointer->label label->pointer
            listbox? pointer->listbox listbox->pointer
            listbox-key? pointer->listbox-key listbox-key->pointer
            scale? pointer->scale scale->pointer
            textbox? pointer->textbox textbox->pointer

            make-double-pointer
            generic-component->pointer
            pointer->pointer-list))

(define-syntax define-libnewt-type
  (lambda (s)
    "Define a wrapped pointer type for an opaque type of libnewt."
    (syntax-case s ()
      ((_ name)
       (let ((symbol     (syntax->datum #'name))
             (identifier (lambda (symbol)
                           (datum->syntax #'name symbol))))
         (with-syntax ((rtd    (identifier (symbol-append '< symbol '>)))
                       (pred   (identifier (symbol-append symbol '?)))
                       (wrap   (identifier (symbol-append 'pointer-> symbol)))
                       (unwrap (identifier (symbol-append symbol '->pointer))))
           #`(define-wrapped-pointer-type rtd
               pred
               wrap unwrap
               (lambda (obj port)
                 (format port "#<newt-~a ~a>"
                         #,(symbol->string symbol)
                         (number->string (pointer-address (unwrap obj))
                                         16))))))))))
(define-libnewt-type component)
(define-libnewt-type checkbox)
(define-libnewt-type checkboxtree)
(define-libnewt-type checkboxtree-key)
(define-libnewt-type entry)
(define-libnewt-type form)
(define-libnewt-type grid)
(define-libnewt-type label)
(define-libnewt-type listbox)
(define-libnewt-type listbox-key)
(define-libnewt-type scale)
(define-libnewt-type textbox)

(define (make-double-pointer)
  (bytevector->pointer (make-bytevector (sizeof '*))))

(define (generic-component->pointer component)
  "Return the underlying pointer of the given COMPONENT."
  (let ((pointer-extractor
         (cond
          ((component? component) component->pointer)
          ((checkbox? component) checkbox->pointer)
          ((label? component) label->pointer)
          ((listbox? component) listbox->pointer)
          ((checkboxtree? component) checkboxtree->pointer)
          ((textbox? component) textbox->pointer)
          ((scale? component) scale->pointer)
          ((grid? component) grid->pointer)
          ((entry? component) entry->pointer))))
    (pointer-extractor component)))

(define (pointer->pointer-list ptr length)
  (let ((size (sizeof '*)))
    (let ((main (pointer->bytevector ptr (* size length))))
        (let loop ((count 0)
                   (acc '()))
          (if (= count length)
              (reverse acc)
              (let* ((offset (* count size))
                     (next-ptr (bytevector->pointer main offset)))
                (loop (+ count 1)
                      (cons (dereference-pointer next-ptr) acc))))))))

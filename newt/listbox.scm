;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt listbox)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-listbox
            append-entry-to-listbox
            current-listbox-entry
            set-current-listbox-entry
            set-current-listbox-entry-by-key
            set-listbox-entry-text
            set-listbox-width
            insert-entry-to-listbox
            delete-listbox-entry
            clear-listbox
            listbox-entries
            listbox-selection))

(define (make-listbox left top height flags)
  "Create a listbox at (LEFT, TOP) coordinates with the given HEIGHT
and FLAGS."
  (let ((proc (libnewt->procedure '*
                                  "newtListbox"
                                  `(,int ,int ,int ,int))))
    (pointer->listbox (proc left top height flags))))

(define (append-entry-to-listbox listbox text)
  "Append the TEXT entry to the given LISTBOX and return its key."
  (let ((proc (libnewt->procedure int
                                  "newtListboxAppendEntry"
                                  '(* * *))))
    (let ((key-argument (make-c-struct (list int) (list 0))))
      (proc (listbox->pointer listbox) (string->pointer text) key-argument)
      (pointer->listbox-key key-argument))))

(define (current-listbox-entry listbox)
  "Return the key of the current entry of LISTBOX."
  (let ((proc (libnewt->procedure '*
                                  "newtListboxGetCurrent"
                                  '(*))))
    (pointer->listbox-key (proc (listbox->pointer listbox)))))

(define (set-current-listbox-entry listbox index)
  "Set the entry at INDEX position as the current entry of the given
LISTBOX."
  (let ((proc (libnewt->procedure void
                                  "newtListboxSetCurrent"
                                  `(* ,int))))
    (proc (listbox->pointer listbox) index)))

(define (set-current-listbox-entry-by-key listbox listbox-key)
  "Set the entry identified by LISTBOX-KEY as the current entry of the
given LISTBOX."
  (let ((proc (libnewt->procedure void
                                  "newtListboxSetCurrentByKey"
                                  '(* *))))
    (proc (listbox->pointer listbox) (listbox-key->pointer listbox-key))))

(define (set-listbox-entry-text listbox index text)
  "Set the text of the entry at INDEX in the given LISTBOX to TEXT."
  (let ((proc (libnewt->procedure void
                                  "newtListboxSetEntry"
                                  `(* ,int *))))
    (proc (listbox->pointer listbox) index (string->pointer text))))

(define (set-listbox-width listbox width)
  "Set the width of the given LISTBOX to WIDTH."
  (let ((proc (libnewt->procedure void
                                  "newtListboxSetWidth"
                                  `(* ,int))))
    (proc (listbox->pointer listbox) width)))

(define (insert-entry-to-listbox listbox text previous-key)
  "Insert a new entry with the given TEXT in LISTBOX, just after the
entry identified by PREVIOUS-KEY."
  (let ((proc (libnewt->procedure int
                                  "newtListboxInsertEntry"
                                  '(* * * *))))
    (let ((key-argument (make-c-struct (list int) (list 0))))
      (proc (listbox->pointer listbox)
            (string->pointer text)
            key-argument
            (listbox-key->pointer previous-key))
      (pointer->listbox-key key-argument))))

(define (delete-listbox-entry listbox listbox-key)
  "Delete the entry identified by LISTBOX-KEY in the given LISTBOX."
  (let ((proc (libnewt->procedure int
                                  "newtListboxDeleteEntry"
                                  '(* *))))
    (proc (listbox->pointer listbox) (listbox-key->pointer listbox-key))))

(define (clear-listbox listbox)
  "Remove all the entries of the given LISTBOX."
  (let ((proc (libnewt->procedure void
                                  "newtListboxClear"
                                  '(*))))
    (proc (listbox->pointer listbox))))

(define (listbox-entries listbox)
  "Return all the entries of the given LISTBOX."
  (let ((proc (libnewt->procedure int
                                  "newtListboxItemCount"
                                  '(*))))
    (proc (listbox->pointer listbox))))

(define (listbox-selection listbox)
  "Return the keys of the selected entries in the given LISTBOX."
  (let ((proc (libnewt->procedure '*
                                  "newtListboxGetSelection"
                                  '(* *))))
    (let* ((c-items (make-c-struct (list int) (list 0)))
           (result (proc (listbox->pointer listbox) c-items))
           (items (car (parse-c-struct c-items (list int)))))
      (if (= items 0)
          '()
          (map pointer->listbox-key
               (pointer->pointer-list result items))))))

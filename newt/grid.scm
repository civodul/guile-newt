;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt grid)
  #:use-module (srfi srfi-1)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (GRID-ELEMENT-EMPTY
            GRID-ELEMENT-COMPONENT
            GRID-ELEMENT-SUBGRID

            make-grid

            vertically-stacked-grid
            vertically-close-stacked-grid
            horizontal-stacked-grid
            horizontal-close-stacked-grid

            basic-window-grid
            simple-window-grid

            ANCHOR-LEFT
            ANCHOR-RIGHT
            ANCHOR-TOP
            ANCHOR-BOTTOM

            GRID-FLAG-GROW-X
            GRID-FLAG-GROW-Y

            set-grid-field
            set-grid-place
            free-grid
            grid-size
            make-wrapped-grid-window
            make-wrapped-grid-window-at-position
            add-form-to-grid))

(define GRID-ELEMENT-EMPTY     0)
(define GRID-ELEMENT-COMPONENT 1)
(define GRID-ELEMENT-SUBGRID   2)

(define (make-grid columns rows)
  "Create a grid of COLUMNS x ROWS. A grid is meant to gather Newt
Components. For example, a grid with 2 columns and 2 rows can host
4 Components at positions (0, 0), (0, 1), (1, 0) and (1, 1)."
  (let ((proc (libnewt->procedure '*
                                  "newtCreateGrid"
                                  `(,int ,int))))
    (pointer->grid (proc columns rows))))

(define (stacked-grid-procedure procedure)
  "Newt functions creating horizontal and vertical stacked grids
expect the following arguments:

    (COMPONENT1-TYPE COMPONENT1 COMPONENT2-TYPE COMPONENT2 ...)

Where COMPONENT-TYPE is:

    GRID-ELEMENT-EMPTY or GRID-ELEMENT-COMPONENT or GRID-ELEMENT-SUBGRID.

This procedure returns a lambda taking a components list (as described
above) as argument. The arity of the C procedure is deduced from this
list. Finally, the given PROCEDURE is called with those components."

  (lambda components
    (let* ((count (/ (length components) 2))
           (prototype-components (fold (lambda (_ acc)
                                         (append `(,int *) acc))
                                       '()
                                       (iota count)))
           (prototype (append prototype-components '(*)))
           (proc (libnewt->procedure '*
                                     procedure
                                     prototype))
           (c-components (map (lambda (component)
                                (if (number? component)
                                    component
                                    (generic-component->pointer component)))
                              components)))
      (pointer->grid (apply proc (append c-components (list %null-pointer)))))))

(define vertically-stacked-grid
  ;; Creates a grid where components are stacked vertically.
  ;; This procedure takes a variable number of arguments, see
  ;; STACKED-GRID-PROCEDURE for the formalism.
  (stacked-grid-procedure "newtGridVStacked"))

(define vertically-close-stacked-grid
  ;; Creates a grid where components are stacked vertically,
  ;; without gap between stacked elements.
  ;; This procedure takes a variable number of arguments, see
  ;; STACKED-GRID-PROCEDURE for the formalism.
  (stacked-grid-procedure "newtGridVCloseStacked"))

(define horizontal-stacked-grid
  ;; Creates a grid where components are stacked horizontally.
  ;; This procedure takes a variable number of arguments, see
  ;; STACKED-GRID-PROCEDURE for the formalism.
  (stacked-grid-procedure "newtGridHStacked"))

(define horizontal-close-stacked-grid
  ;; Creates a grid where components are stacked horizontally,
  ;; without gap between stacked elements.
  ;; This procedure takes a variable number of arguments, see
  ;; STACKED-GRID-PROCEDURE for the formalism.
  (stacked-grid-procedure "newtGridHCloseStacked"))

(define (basic-window-grid text-component middle-grid button-grid)
  "Create a grid where the given TEXT-COMPONENT, and the subgrids
MIDDLE-GRID and BUTTON-GRID are stacked vertivally."
  (let ((proc (libnewt->procedure '*
                                  "newtGridBasicWindow"
                                  '(* * *))))
    (pointer->grid
     (proc (generic-component->pointer text-component)
           (grid->pointer middle-grid)
           (grid->pointer button-grid)))))

(define (simple-window-grid text-component middle-component button-grid)
  "Create a grid where the given TEXT-COMPONENT, MIDDLE-COMPONENT and
the subgrid BUTTON-GRID are stacked vertically."
  (let ((proc (libnewt->procedure '*
                                  "newtGridSimpleWindow"
                                  '(* * *))))
    (pointer->grid
     (proc (generic-component->pointer text-component)
           (generic-component->pointer middle-component)
           (grid->pointer button-grid)))))

(define ANCHOR-LEFT   1)
(define ANCHOR-RIGHT  2)
(define ANCHOR-TOP    4)
(define ANCHOR-BOTTOM 8)

(define GRID-FLAG-GROW-X 1)
(define GRID-FLAG-GROW-Y 2)

(define* (set-grid-field grid column row element component
                         #:key
                         (pad-left 0) (pad-top 0) (pad-right 0) (pad-bottom 0)
                         (anchor 0) (flags 0))
  "Add COMPONENT to the given GRID at (COLUMN, ROW)
coordinates. ELEMENT is equal to GRID-ELEMENT-EMPTY,
GRID-ELEMENT-COMPONENT or GRID-ELEMENT-SUBGRID. PAD-LEFT, PAD-TOP,
PAD-RIGHT and PAD-BOTTOM indicate the padding between elements. ANCHOR
can be 0 for no anchor, or one of the anchor flags above. FLAGS can be
0 or one of the grid flags defined above."
  (let ((proc (libnewt->procedure void
                                  "newtGridSetField"
                                  `(* ,int ,int ,int * ,int ,int ,int
                                      ,int ,int ,int))))
    (proc (grid->pointer grid)
          column row
          element (generic-component->pointer component)
          pad-left pad-top pad-right pad-bottom
          anchor flags)))

(define (set-grid-place grid left top)
  "Place the given GRID at (LEFT, TOP) coordinates of the active
window."
  (let ((proc (libnewt->procedure void
                                  "newtGridPlace"
                                  `(* ,int ,int))))
    (proc (grid->pointer grid) left top)))

(define (free-grid grid recurse?)
  "Destroy the given GRID and all the child components if RECURSE? is
set to #t."
  (let ((proc (libnewt->procedure void
                                  "newtGridFree"
                                  `(* ,int))))
    (proc (grid->pointer grid) (if recurse? 1 0))))

(define (grid-size grid)
  "Return the width and heigth of the given GRID."
  (let ((proc (libnewt->procedure void
                                  "newtGridGetSize"
                                  '(* * *))))
    (let ((width (make-c-struct (list int) (list 0)))
          (height (make-c-struct (list int) (list 0))))
      (proc (grid->pointer grid) width height)
      (values (parse-c-struct width (list int))
              (parse-c-struct height (list int))))))

(define (make-wrapped-grid-window grid title)
  "Create a window wrapping GRID and with the given TITLE."
  (let ((proc (libnewt->procedure void
                                  "newtGridWrappedWindow"
                                  '(* *))))
    (proc (grid->pointer grid) (string->pointer title))))

(define (make-wrapped-grid-window-at-position grid title left top)
  "Create a window wrapping GRID and with the given TITLE at (LEFT,
TOP) coordinates."
  (let ((proc (libnewt->procedure void
                                  "newtGridWrappedWindowAt"
                                  `(* * ,int ,int))))
    (proc (grid->pointer grid) (string->pointer title) left top)))

(define (add-form-to-grid grid form recurse?)
  "Add all the components of GRID, and all its child components if
RECURSE? is set to #t, to the given FORM."
  (let ((proc (libnewt->procedure void
                                  "newtGridAddComponentsToForm"
                                  `(* * ,int))))
    (proc (grid->pointer grid) (form->pointer form) (if recurse? 1 0))))

;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt checkboxtree)
  #:use-module (system foreign)
  #:use-module (newt bindings)
  #:use-module (newt types)
  #:export (make-checkboxtree
            add-entry-to-checkboxtree
            current-checkbox-selection
            set-current-checkboxtree-entry
            set-checkboxtree-entry-text
            set-checkboxtree-width
            checkboxtree-entry-value
            set-checkboxtree-entry-value))

(define (make-checkboxtree left top height flags)
  (let ((proc (libnewt->procedure '*
                                  "newtCheckboxTree"
                                  `(,int ,int ,int ,int))))
    (pointer->checkboxtree (proc left top height flags))))

(define ARG-LAST   -100000)
(define ARG-APPEND -1)

(define (add-entry-to-checkboxtree checkboxtree text flags)
  (let ((proc (libnewt->procedure int
                                  "newtCheckboxTreeAddItem"
                                  `(* * * ,int ,int ,int))))
    (let ((key-argument (make-c-struct (list int) (list 0))))
      (proc (checkboxtree->pointer checkboxtree)
            (string->pointer text)
            key-argument
            flags
            ARG-APPEND
            ARG-LAST)
      (pointer->checkboxtree-key key-argument))))

(define (current-checkbox-selection checkboxtree)
  (let ((proc (libnewt->procedure '*
                                  "newtCheckboxTreeGetSelection"
                                  '(* *))))
    (let* ((c-items (make-c-struct (list int) (list 0)))
           (result (proc (checkboxtree->pointer checkboxtree) c-items))
           (items (car (parse-c-struct c-items (list int)))))
      (if (= items 0)
          '()
          (map pointer->checkboxtree-key
               (pointer->pointer-list result items))))))

(define (set-current-checkboxtree-entry checkboxtree checkboxtree-key)
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxTreeSetCurrent"
                                  '(* *))))
    (proc (checkboxtree->pointer checkboxtree)
          (checkboxtree-key->pointer checkboxtree-key))))

(define (set-checkboxtree-entry-text checkboxtree checkboxtree-key text)
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxTreeSetEntry"
                                  `(* * *))))
    (proc (checkboxtree->pointer checkboxtree)
          (checkboxtree-key->pointer checkboxtree-key)
          (string->pointer text))))

(define (set-checkboxtree-width checkboxtree width)
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxTreeSetWidth"
                                  `(* ,int))))
    (proc (checkboxtree->pointer checkboxtree) width)))

(define (checkboxtree-entry-value checkboxtree checkboxtree-key)
  (let ((proc (libnewt->procedure int8
                                  "newtCheckboxTreeGetEntryValue"
                                  '(* *))))
    (integer->char (proc (checkboxtree->pointer checkboxtree)
                         (checkboxtree-key->pointer checkboxtree-key)))))

(define (set-checkboxtree-entry-value checkboxtree checkboxtree-key value)
  (let ((proc (libnewt->procedure void
                                  "newtCheckboxTreeSetEntryValue"
                                  `(* * ,int8))))
    (proc (checkboxtree->pointer checkboxtree)
          (checkboxtree-key->pointer checkboxtree-key)
          (char->integer value))))

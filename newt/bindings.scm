;;; Guile-newt --- GNU Guile bindings of libnewt
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-newt.
;;;
;;; Guile-Git is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-Git is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Git.  If not, see <http://www.gnu.org/licenses/>.

(define-module (newt bindings)
  #:use-module (system foreign)
  #:use-module (newt config)
  #:export (libnewt
            libnewt->procedure
            libnewt->procedure*

            newt-init
            newt-finish
            newt-delay

            COLORSET-ROOT
            COLORSET-BORDER
            COLORSET-WINDOW
            COLORSET-SHADOW
            COLORSET-TITLE
            COLORSET-BUTTON
            COLORSET-ACTBUTTON
            COLORSET-CHECKBOX
            COLORSET-ACTCHECKBOX
            COLORSET-ENTRY
            COLORSET-LABEL
            COLORSET-LISTBOX
            COLORSET-ACTLISTBOX
            COLORSET-TEXTBOX
            COLORSET-ACTTEXTBOX
            COLORSET-HELPLINE
            COLORSET-ROOTTEXT
            COLORSET-EMPTYSCALE
            COLORSET-FULLSCALE
            COLORSET-DISENTRY
            COLORSET-COMPACTBUTTON
            COLORSET-ACTSELLISTBOX
            COLORSET-SELLISTBOX

            newt-set-color

            newt-refresh
            newt-suspend
            newt-resume))

(define libnewt
  (dynamic-link %libnewt))

(define (libnewt->procedure return name params)
  (pointer->procedure return (dynamic-func name libnewt) params))

(define-inlinable (libnewt->procedure* name params)
  (let ((proc (libnewt->procedure int name params)))
    (lambda args
      (apply proc args))))

(define (newt-init)
  "This function has to be called before any other of the library. It
initializes internal data structures and places the terminal in raw
mode."
  (let ((proc (libnewt->procedure int "newtInit" '())))
    (proc)))

(define (newt-finish)
  "This function should be called when the program is ready to
exit. It restores the terminal to its original appearance."
  (let ((proc (libnewt->procedure int "newtFinished" '())))
    (proc)))

(define (newt-delay duration-us)
  "Pauses execution for DURATION-US microseconds."
  (let ((proc (libnewt->procedure void "newtDelay" `(,unsigned-int))))
    (proc duration-us)))

(define COLORSET-ROOT            2)
(define COLORSET-BORDER          3)
(define COLORSET-WINDOW          4)
(define COLORSET-SHADOW          5)
(define COLORSET-TITLE           6)
(define COLORSET-BUTTON          7)
(define COLORSET-ACTBUTTON       8)
(define COLORSET-CHECKBOX        9)
(define COLORSET-ACTCHECKBOX    10)
(define COLORSET-ENTRY          11)
(define COLORSET-LABEL          12)
(define COLORSET-LISTBOX        13)
(define COLORSET-ACTLISTBOX     14)
(define COLORSET-TEXTBOX        15)
(define COLORSET-ACTTEXTBOX     16)
(define COLORSET-HELPLINE       17)
(define COLORSET-ROOTTEXT       18)
(define COLORSET-EMPTYSCALE     19)
(define COLORSET-FULLSCALE      20)
(define COLORSET-DISENTRY       21)
(define COLORSET-COMPACTBUTTON  22)
(define COLORSET-ACTSELLISTBOX  23)
(define COLORSET-SELLISTBOX     24)

(define (newt-set-color colorset foreground background)
  "Set given FOREGROUND and BACKGROUND colors for given COLORSET."
  (let ((proc (libnewt->procedure void "newtSetColor" `(,int * *))))
    (proc colorset
          (string->pointer foreground) (string->pointer background))))

(define (newt-refresh)
  "To increase performance, Newt will only updates the display right
before the user is exepected to press a key. This function is useful
if you want to display progress messages without forcing the user to
input character."
  (let ((proc (libnewt->procedure void "newtRefresh" '())))
    (proc)))

(define (newt-suspend)
  "Restore the terminal to its initial state, allowing for forking
child program for instance. NEWT-RESUME has to be called to restore
Newt user interface."
  (let ((proc (libnewt->procedure void "newtSuspend" '())))
    (proc)))

;; TODO: newtSetSuspendCallback

;; TODO: newtSetHelpCallback

(define (newt-resume)
  "Restore the Newt user interface after a call to NEWT-SUSPEND."
  (let ((proc (libnewt->procedure int "newtResume" '())))
    (proc)))
